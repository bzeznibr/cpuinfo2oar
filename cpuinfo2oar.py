#!/usr/bin/env python
# coding: utf-8

import subprocess, os, sys

def get_cpuinfo(f_path):
    """ return useful lists from /pro/cpuinfo file"""
    command = "cat " + f_path
    core_list, proc_list, phys_list = [], [], []
    all_info = subprocess.check_output(command, shell=True).strip()

    # Get processor, physical and core ids from cpuinfo
    for line in all_info.decode().split("\n"):
        if "core id" in line:
            core_list.append(int(line.split(':')[1].replace(' ','')))
        elif "processor" in line:
            proc_list.append(int(line.split(':')[1].replace(' ','')))
        elif "physical id" in line:
            phys_list.append(int(line.split(':')[1].replace(' ','')))

    return phys_list, proc_list, core_list


def get_oarinfo(f_path):
    """return cpu numbers from oar for each core"""
    command = "cat " + f_path
    all_info = subprocess.check_output(command, shell=True).strip()
    oar_list = []
    for line in all_info.decode().split('\n'):
        oar_list.append(int(line.split(':')[1]))

    return oar_list


def associate_info(oar_list, phys_list, proc_list, core_list):
    """Associate cpu numbers from oar to correct cpu numbers
    from /proc/cpuinfo"""
    cores = list(set(phys_list))
    n_cores = len(cores)
    c_num = []
    for c in cores:
        ind = [i for i in range(len(phys_list)) if phys_list[i] == c]
        n = int(len(oar_list)/n_cores)
        for i in ind[:n]:
            c_num.append(proc_list[i])

    out = dict(zip(oar_list, c_num))
    print(out)


if __name__ == "__main__":
    phys_list, proc_list, core_list = get_cpuinfo(sys.argv[1])
    associate_info(get_oarinfo(sys.argv[2]), phys_list, proc_list, core_list)
